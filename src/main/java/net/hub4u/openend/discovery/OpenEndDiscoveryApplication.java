package net.hub4u.openend.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@EnableEurekaServer
@SpringBootApplication
public class OpenEndDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenEndDiscoveryApplication.class, args);
	}

}
